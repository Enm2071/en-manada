import React from 'react';
import './App.css';
import LayOut from './hoc/LayOut/LayOut';

function App() {
  return (
    <LayOut />
  );
}

export default App;
